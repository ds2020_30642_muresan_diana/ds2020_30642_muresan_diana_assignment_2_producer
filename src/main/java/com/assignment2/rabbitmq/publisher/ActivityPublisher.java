package com.assignment2.rabbitmq.publisher;


import com.assignment2.rabbitmq.RabbitMqApplication;
import com.assignment2.rabbitmq.FileParser;
import com.assignment2.rabbitmq.dto.Activity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanNameAware;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.scheduling.annotation.ScheduledAnnotationBeanPostProcessor;
import org.springframework.stereotype.Service;


import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;


@Service
public class ActivityPublisher implements ApplicationContextAware, BeanNameAware {
    private ApplicationContext applicationContext;
    private String beanName;
    private static final Logger log = LoggerFactory.getLogger(ActivityPublisher.class);
    private final RabbitTemplate rabbitTemplate;
    public static final List<String> activities = FileParser.readFile("D:\\diverse\\an4\\SD\\Assignment2\\ds2020_30642_muresan_diana_assignment_2_producer\\src\\main\\java\\com\\assignment2\\rabbitmq\\activity.txt");
    public static int i = 0;






    public ActivityPublisher(final RabbitTemplate rabbitTemplate ) {
        this.rabbitTemplate = rabbitTemplate;


    }

    private void stopScheduledTask()
    {
        ScheduledAnnotationBeanPostProcessor bean = applicationContext.getBean(ScheduledAnnotationBeanPostProcessor.class);
        bean.postProcessBeforeDestruction(this, beanName);
    }

    @Scheduled(fixedDelay = 1000L)
    public void sendActivity()   {


        if (i == activities.size()) {
            stopScheduledTask();
        }
        else {
            String[] values = activities.get(i).split("\t\t");


            UUID patientId = UUID.fromString("2e42c6f0-bda2-4e7d-977e-10f52548955c");
            String startTime = values[0];
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            long start = 0;
            long end = 0;

            try {
                Date date = sdf.parse(startTime);
                start = date.getTime();
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }

            String endTime = values[1];
            SimpleDateFormat sdfe = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            try {
                Date date = sdfe.parse(endTime);
                end = date.getTime();
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
            String[] act = values[2].split("\t");
            Activity activity = new Activity(patientId, act[0], start, end);
            rabbitTemplate.convertAndSend(RabbitMqApplication.exchange, RabbitMqApplication.routingKey, activity);
            log.info("Activity sent");
            i++;

        }

    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }

    @Override
    public void setBeanName(String s) {
        this.beanName = beanName;
    }
}
