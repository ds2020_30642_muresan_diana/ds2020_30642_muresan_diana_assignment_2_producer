package com.assignment2.rabbitmq.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;


import java.io.Serializable;
import java.util.UUID;


public class Activity implements Serializable {


    private UUID patientId;
    private String activity;
    private Long start;
    private Long end;

    public Activity(
                    @JsonProperty("patientId") final UUID patientId,
                    @JsonProperty("activity") final String activity,
                    @JsonProperty("start") final Long start,
                    @JsonProperty("end") final Long end) {

        this.patientId = patientId;
        this.activity = activity;
        this.start = start;
        this.end = end;
    }



    public UUID getPatientId() {
        return patientId;
    }

    public void setPatientId(UUID patientId) {
        this.patientId = patientId;
    }

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public Long getStart() {
        return start;
    }

    public void setStart(Long start) {
        this.start = start;
    }

    public Long getEnd() {
        return end;
    }

    public void setEnd(Long end) {
        this.end = end;
    }

    @Override
    public String toString() {
        return "Activity{" +

                ", patientId=" + patientId +
                ", activity='" + activity + '\'' +
                ", start=" + start +
                ", end=" + end +
                '}';
    }
}

