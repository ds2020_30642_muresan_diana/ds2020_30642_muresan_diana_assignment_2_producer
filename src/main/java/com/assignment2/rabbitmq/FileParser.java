package com.assignment2.rabbitmq;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class FileParser {

    public static List<String>  readFile(String filename) {
        List<String> lines = new ArrayList<String>();
        try {

            BufferedReader br = new BufferedReader(new FileReader(filename));
            String line;
            while ((line = br.readLine()) != null) {
                System.out.println(line);
                lines.add(line);
            }
            br.close();

        } catch (IOException ex) {
            ex.printStackTrace();
        }

        return lines;
    }


}
